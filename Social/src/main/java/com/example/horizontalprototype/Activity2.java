package com.example.horizontalprototype;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.annotation.SuppressLint;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Activity2 extends AppCompatActivity {

    TextView cups;
    Button btnClick, btnReset,btnSave;
    DatabaseHelper mDatabaseHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        @SuppressLint("WrongViewCast") ConstraintLayout constraintLayout = findViewById(R.id.layout);
        AnimationDrawable animationDrawable = (AnimationDrawable) constraintLayout.getBackground();
        animationDrawable.setEnterFadeDuration(2000);
        animationDrawable.setExitFadeDuration(4000);
        animationDrawable.start();

        cups = (TextView) findViewById(R.id.cups);
        btnClick = (Button) findViewById(R.id.btnClick);
        btnReset = (Button) findViewById(R.id.btnRest);
        btnSave = (Button) findViewById(R.id.btnSave);
        mDatabaseHelper = new DatabaseHelper(this);

        btnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String countValue = cups.getText().toString();

                int intCountValue = Integer.parseInt(countValue);
                intCountValue++;

                cups.setText(String.valueOf(intCountValue));


            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cups.setText(String.valueOf(0));
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String value = cups.getText().toString();

               if(cups.length() !=0){
                   AddData(value);
               }else{
                   toastMessage("Error");
               }
            }
        });
    }

    public void AddData(String newEntry){
        boolean insertData =mDatabaseHelper.addData(newEntry);
        if (insertData){
            toastMessage("Data successfully Inserted!");
        }else{
            toastMessage("Something went wrong");
        }
    }

    private void toastMessage(String message){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }
}