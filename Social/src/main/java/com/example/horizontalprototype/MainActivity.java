package com.example.horizontalprototype;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private Button button3;
    private Button button1;
    private Button button4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button4 = (Button) findViewById(R.id.button4);
        button4.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View v) {
                                           social();}});

        button3 = (Button) findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intakeoftheday();

            }
        }
        );

        button1 = (Button) findViewById(R.id.button1);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                History();
            }
        });
    }

    public void Intakeoftheday() {
        Intent intent = new Intent(this, Activity2.class);
        startActivity(intent);
    }

    public void History(){
        Intent intent = new Intent( this, MainActivity3.class );
        startActivity(intent);
    }

    public void social(){
        Intent intent = new Intent(this, Activity4.class);
        startActivity(intent);
    }
}